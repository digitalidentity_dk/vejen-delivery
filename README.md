# Overview
The project consists of two components, AppCitizen and AppEmployee.

Both applications are Spring Boot applications, and comes bundled with an embedded Tomcat for easy deployment

# Installation Notes
Each project has a config folder, containing (at least) the following files

```
.
├── client.properties
├── logback.xml
├── sts.properties
├── sts.wsdl
└── vejen.properties
```

The client.properties, sts.properties and sts.wsdl files are CXF configuration files used to interact with the DDL STS to retrieve a token.
The content of these files can likely be kept "as is", unless keystores are renamed, or passwords for these are changed.

The logback.xml file contains a useful logback configuration, and can be modified if needed.

The vejen.properties file contains the full application configuration, and is described in the chapter below.

In the AppCitizen project, one additional file exists, called pdf-fetcher.js. This file contains a small phantom-js script, that is used by
the AppCitizen project to generate PDF files. This script contains a hardcoded path to the place where the AppCitizen project is deployed,
and should be updated to reflect the installation URL.

## vejen.properties
The two applications have almost the same configuration file. Where they differ it is mentioned below

```
# Of these settings, only the url, username and password part is truly important.
# The datasource must exists already as a MySQL database, and a Flyway script will
# ensure that all the correct tables are created
dataSource.driverClassName=com.mysql.jdbc.Driver
dataSource.password=Test1234
dataSource.username=root
dataSource.url=jdbc:mysql://localhost:3306/telemedicine
dataSource.maxActive=20
dataSource.minIdle=1
dataSource.maxIdle=5
dataSource.initialSize=2
dataSource.testWhileIdle=true
dataSource.testOnBorrow=true
dataSource.validationQuery=SELECT 1
dataSource.validationInterval=30000
dataSource.maxAge=3600000

# Setup the port and contextPath for the application. I suggest leaving these as is, and instead use the
# httpd reverse proxy to map these. Note that the reverse proxy should only map the port, but leave
# the contextPath unchanged, otherwise Spring breaks in various places
server.port=8081
server.contextPath=/citizen

# This is the SAML 2.0 configuration. The configuration must very likely be updated
saml.baseUrl=http://vejen.digital-identity.dk:8081/citizen
saml.entityid=http://vejen.digital-identity.dk:8081/citizen/saml/metadata
saml.keystore.location=file:config/security/samlKeystore.jks
saml.keystore.password=Test1234
saml.keystore.alias=selfsigned
saml.page.success=/
saml.page.error=/error

# when running behind a reverse proxy, the following SAML settings are important (they are used together with SAMLContextProviderLB)
saml.proxy.scheme=http
saml.proxy.servername=vejen.digital-identity.dk
saml.proxy.port=8081
saml.proxy.includeport=true
saml.proxy.contextpath=/citizen

# These settings are used to restrict which type of user can login to the application
# Leave the municipality field blank for AppCitizen. The role should either be CITIZEN or MUNICIPALITY
saml.municipality=561
saml.role=CITIZEN

# This is the magical logout URL on the DDL IdP used for SLO. Note that the return argument points
# to a URL on the appication, and should be updated to reflect where the application is actually
# deployed
saml.logoutUrl=https://ddlsecurity.nextstepcitizen.dk/Shibboleth.sso/Logout?return=http%3A%2F%2Fvejen.digital-identity.dk%3A8081%2Fcitizen

# These settings are ONLY used on the AppCitizen project, and points to the location of both the phantomjs binary and the script
phantomjs.bin=config/phantomjs
phantomjs.script=config/pdf-fetcher.js
```

## PhantomJS dependency
The AppCitizen project requires PhantomJS to be installed on the machine. As mentioned in the vejen.properties section above,
the property file must point to the phantomjs binary.

## security folder
In the config folder, there is a security folder. This folder contains all the keystores used by the application. Those are

```
.
├── client.jks
├── samlKeystore.jks
├── sts-trust.jks
```

The client.jks keystore contains the keys used to interact with the STS. This keystore should be updated upon installation, and the DDL STS
should be configured to trust this specific key (and allow it to get a token to call the Vejen Data Services application).

The sts-trust.jks is the trust-store for the application, and it contains the DDL STS certificate, as well as any SSL certificates that needs
to be trusted by the application. Any changes to 3rd party certificates should be relected in this trust-store.

Finally the samlKeystore.jks is the keystore that the application uses to identify itself against the Identity Provider. The applications must
have different keys here, otherwise the Identity Provider will not know which application is calling it.

## metadata folder
In the config folder, there is a metadata folder. Place a file here called idp.xml, which contains the Identity Provider metadata.

## Firewall setup
One of the endpoints exposed by the AppCitizen application is only for internal use by the application itself. It should not be exposed to the
internet. Either prevent access in the firewall, or through some reverse proxy mapping in httpd.

All endpoints that are mapped to /internal/ should be firewalled off. E.g.

http://vejen.digital-identity.dk:8081/citizen/internal/

## Getting metadata and exchanging with the DDL Identity Provider
Once the application has been deployed, it is possible to get the application metadata by accessing the metadata URL on the application, e.g.

http://vejen.digital-identity.dk:8081/citizen/saml/metadata

This metadata must be given to the DDL Identity Provider, so it can be registered as a Service Provider.

# Running the applications
The applications comes with a small script to start the application called run.sh. This basically just starts the Java application with
a few VM arguments (need a bit more memory, and need to set the config folder on the classpath).

```
$ ./run.sh &
```
