var system = require('system');
var citizenId = system.args[1];
var gotoDate = system.args[2];
var fileName = system.args[3];

var page = require('webpage').create();

page.paperSize = { format: 'A4',  orientation: 'Portrait', margin: '1cm'};

page.open('http://vejen.digital-identity.dk:8081/citizen/internal/overview?citizenId=' + citizenId + '&date=' + gotoDate, function() {
  page.render(fileName);
  phantom.exit();
});
